

# Guide d'installation du POC de gestion des Logs avec ELK

## Les pré requis



Il faut auparavant installer :

- Vagrant
- les plugins vagrant utilisés : vagrant-hostmanager, vagrant-hosts
- VirtualBox
- Ansible
- Le playbook elastc

Pour les plugins https://github.com/hashicorp/vagrant/wiki/Available-Vagrant-Plugins

Pour le playbook elastic https://github.com/elastic/ansible-elasticsearch

En option installer le plugin chrome elasticsearch-head https://chrome.google.com/webstore/detail/elasticsearch-head/ffmkiejjmecolpfloofpjologoblkegm

## Mise en route des VM

Clone le worspace

```bash
git clone https://gitlab.com/sec-data-analysis/logs-elk.git
```

Nous pouvons modifier les IP des VM dans le vagrant file.

Lancer les VM 

```
vagrant up
```

Vérifier les incohérences des fichiers /etc/host pour les VM et modifier-les comme le montre la vidéo.

## Alimenter les VM

Penser à changer les clés ssh des machines. Configurer le fichier hosts nécessaire à Ansible en fonction de vos specs réseau.

Assurez-vous d'avoir bien installé le playbook elastic puis :

```
ansible-playbook -i hosts playbook.yml
```


## Installer elast'alert

Se positionner dans la machine:

```
vagrant ssh ealert
```

Puis pour l'installation :

```bash
sudo apt install python3-pip
pip3 install elastalert==0.2.4
pip3 install elasticsearch==7.0.0 
pip3 install "setuptools>=11.3"
git clone https://github.com/Yelp/elastalert
cd elastalert 
python3.6 setup.py install

```

Les règles se situent dans le dossier config du projet.

Vous pouvez les copier coller ou créer un dossier partagé dans le vagrant file.

Sur la machine ealert modifier le fichier config.yml.example présent dans elastalert en config.yml (en configurant la base elastic et le dossier de vos règles).

Créer les indices elast alert puis le démarrer :

```
elastalert-create-index --config config.yml 
elastalert --config config.yml 
```

## Configurer Metricbeat et Kibana

Télécharger Metricbeat suivant votre OS à l'adresse suivante : https://www.elastic.co/fr/downloads/past-releases/metricbeat-7-7-0


Télécharger Kibana suivant votre OS à l'adresse suivante : https://www.elastic.co/fr/downloads/past-releases/kibana-7-7-0

Modifier les fichier de configuration metricbeat.yml et kibana.yml pour paramétrer la base elasticsearch (192.168.1.200 dans notre exemple).

Décompresser les archives en les renomant respectivement (metricbeat et kibana), puis :

```bash
./metricbeat/metricbeat -e -c metricbeat/metricbeat.yml
./kibana/bin/kibana
```